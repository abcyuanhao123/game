#pragma once
template<class T>
class cloud
{
public:
	struct sprite* createCloud()
	{
		cloud_3x2* pCloud_3x2 = (T*)malloc(sizeof(cloud_3x2));
		cloud_3x2Init(pCloud_3x2);
		return (struct sprite*)pCloud_3x2;
	}

	void cloudDraw(T& c)
	{
		putTransparentImage(c->super.x, c->super.y, c->imgCloud_3x2_mask, c->imgCloud_3x2);
	}

	void cloud_3x2UpdateT& c)
	{

	}

	void cloudDestroy(T& c)
	{
		delete c->imgCloud_3x2;
		delete c->imgCloud_3x2_mask;
	}

	void cloudInit(T& c)
	{
		spriteInit((sprite*)c);
		c->super.draw = (void (*)(struct sprite*))cloud_3x2Draw;
		c->super.update = (void (*)(struct sprite*))cloud_3x2Update;
		c->super.destroy = (void (*)(struct sprite*))cloud_3x2Destroy;
		c->super.width = 3 * GRID_WIDTH;
		c->super.height = 2 * GRID_HEIGHT;

		c->imgCloud_3x2 = new IMAGE;
		loadimage(c->imgCloud_3x2, "img/cloud_3x2.png");

		c->imgCloud_3x2_mask = new IMAGE;
		loadimage(c->imgCloud_3x2_mask, "img/cloud_3x2_mask.png");
	}

};
