#include"标头.h"
int main()
{
	srand(time(NULL));
	InitGreaph();
	list<ball*> pball;
	BeginBatchDraw();//批量绘图开始

	LARGE_INTEGER startCount, endCount, F;//开始时间，结束时间，频率
	QueryPerformanceFrequency(&F);//获取计数器累加频率,（一秒加多少次）

	ExMessage msg;

	while (1)
	{
		QueryPerformanceCounter(&startCount);//获取当前计数值
		cleardevice();//清空前面的窗口内容
		//准备图片
		int num = 0;
		if (pball.size() < 5)
		{
			pball.push_back(new ball);//添加新的球
		}
		auto it = pball.begin();
		while(it != pball.end())
		{
			(*it)->push_ball();
			(*it)->move_ball();//绘图+移动
			if ((*it)->y <= 0)
			{
				it = pball.erase(it);//如果超范围就删除
			}
			else
			it++;
		}
		
		QueryPerformanceCounter(&endCount);//获取终止计数值
		long long elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;//获取时间差，单位微秒
		timeBeginPeriod(1);//调整系统的时钟分辨率，让Sleep函数的最小调用间隔为1毫秒，要不然默认为15毫秒
		while (elapse < 20000)//间隔时间小于20毫秒（每秒50针）
		{
			// 鼠标消息获取,放到这里是让鼠标帧数多一些，画面延迟小
			peekmessage(&msg);

			Sleep(1);//为了降低cpu使用
			QueryPerformanceCounter(&endCount);//获取终止计数值
			elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;
		}
		push_mouse(msg);
		kill_ball(msg, pball);
		FlushBatchDraw();//绘出准备好的图片
	}
	timeEndPeriod(1);//恢复时钟分辨率
	EndBatchDraw();//批量绘图结束
	return 0;
}