#include"标头.h"
void InitGreaph()//初始化窗口，创建
{
	initgraph(len, high);
	setbkcolor(WHITE);
	cleardevice();
}
void push_mouse(ExMessage& msg)
{
	switch (msg.message)
	{
	case WM_MOUSEMOVE://鼠标移动
		setlinecolor(RGB(237, 178, 29));//设置描边颜色
		setlinestyle(PS_SOLID, 3);//设置线段样式
		circle(msg.x, msg.y, 20);
		line(msg.x - 20, msg.y, msg.x + 20, msg.y);
		line(msg.x, msg.y - 20, msg.x, msg.y + 20);
		break;
	}
	setlinecolor(WHITE);
}
void kill_ball(ExMessage& msg, list<ball*>& pball)
{
	if (msg.message == WM_LBUTTONDOWN)
	{
		auto it = pball.begin();
		while (it != pball.end())
		{
			int d = pow(msg.x - (*it)->x, 2) + pow(msg.y - (*it)->y, 2);
			if (d <= pow((*it)->r, 2))
			{
				it = pball.erase(it);//如果在圆范围就删除
				break;
			}
			else
				it++;
		}
	}
}

