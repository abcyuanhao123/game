#pragma once
#include<iostream>
#include<easyx.h>
#include <conio.h>
#include<time.h>
#include<vector>
#include<list>
#include<math.h>
using namespace std;
#define len 800
#define high 600
class ball
{
public:
	//srand(time(NULL));
	void push_ball()
	{
		setfillcolor(color);
		fillcircle(x, y, r);
	}
	void move_ball()
	{
		y -= v;//向上移动
	}
	int r = rand() % 10 + 20;//半径10-19
	int x = rand() % (len-20) + 20;//初始x坐标
	int y =  high;//初始坐标，在最底下
private:
	int v = rand() % 3 + 1;//初始速度;
	COLORREF color = HSVtoRGB((float)(rand() % 360), 0.8f, 0.9f);//颜色
};
void InitGreaph();//初始化窗口，创建
void push_mouse(ExMessage& msg);//绘制鼠标
void kill_ball(ExMessage& msg, list<ball*>& pball);//删除气球
