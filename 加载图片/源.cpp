#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<easyx.h>
using namespace std;
#define BEAR_FRAMES 11
void putTransparentImage(int x,int y,IMAGE& a,IMAGE& b)
{
	putimage(x, y, &a, SRCAND);//第三个参数是进行 三光栅计算 与背景图片与运算
	putimage(x, y, &b, SRCPAINT);//进行与运算
}
int main()
{
	initgraph(1200, 480);
	setbkcolor(WHITE);
	cleardevice();

	IMAGE imgBack,imgMask,imgBear;
	loadimage(&imgBack, "C:/Users/03羊/Pictures/Saved Pictures/{e32a6507-d21b-42e0-be1d-7f18b53e8689}.png",1280,480);//背景图片
	//loadimage(&imgMask, "D:/03羊/C语言小游戏实战/源码和图片/代码/20.透明图片/20.透明图片/mask.jpg");//剪影图片
	//loadimage(&imgBear, "D:/03羊/C语言小游戏实战/源码和图片/代码/20.透明图片/20.透明图片/bear.jpg");//熊图片
	//黑色二进制颜色全是0，白色全是1
	//进行与或的光栅运算，剪影图片与背景图片与运算，黑色部分还是黑色，白色部分变成背景颜色
	//在拿实际图片（黑色背景的），与背景图片进行或运算，中间黑色的就变成人物应该有的了
	//剪影图片 and 背景图 or 黑背景人物主体 == 人物+正确背景
	//putimage(0,0,&imgBack);
	//putimage(530,180,&imgMask,SRCAND);//第三个参数是进行 三光栅计算 与背景图片与运算
	//putimage(530,180,&imgBear,SRCPAINT);//进行与运算

	//把每一帧图片都使用IMAGE结构体储存起来
	IMAGE imgArrBearFrames[BEAR_FRAMES];
	for (int i = 0; i < BEAR_FRAMES; i++)
	{
		char str[100];
		sprintf(str, "./frames/bear%d.png", i);
		loadimage(&imgArrBearFrames[i], str);
	}
	IMAGE imgArrBearMasks[BEAR_FRAMES];
	for (int i = 0; i < BEAR_FRAMES; i++)
	{
		char str[100];
		sprintf(str, "./masks/bearmask%d.png", i);
		loadimage(&imgArrBearMasks[i], str);
	}

	int frame = 0;
	int x = 0;
	BeginBatchDraw();//开始批量绘图
	while (1)
	{
		cleardevice();
		putimage(0, 0, &imgBack);
		putTransparentImage(x, 180, imgArrBearMasks[frame], imgArrBearFrames[frame]);

		FlushBatchDraw();//打印出当前储存的图片

		x += 10;
		if (x > 1200)
			x = 0;
		Sleep(50);
		frame++;
		if (frame >= 11)
			frame = 0;
	}
	getchar();
	return 0;
}