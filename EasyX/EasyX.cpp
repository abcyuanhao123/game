﻿#include <iostream>
#include<easyx.h>
using namespace std;
#define PI 3.1415
int main()
{
	initgraph(1000, 1000);//创建窗口
	setbkcolor(WHITE);//设置窗口颜色
	cleardevice();//刷新窗口
	//Sleep(1000);

	setlinecolor(BLACK);//设置 边 颜色
	setlinestyle(PS_SOLID, 10);//设置 边 类型 像素大小
	setfillcolor(BLUE);//设置 填充 颜色
	//Sleep(1000);

	fillellipse(118, 125, 990, 931);//绘制 填充 椭圆

	setfillcolor(WHITE);//设置 填充 颜色
	fillellipse(189, 271, 919, 931);

	fillellipse(375, 170, 555, 420);
	fillellipse(555, 170, 735, 420);
	//Sleep(1000);

	setfillcolor(BLACK);
	solidcircle(484, 333, 25);//填充 圆
	solidcircle(617, 333, 25);//填充 圆
	//Sleep(1000);

	setfillcolor(WHITE);
	solidcircle(484, 333, 10);
	solidcircle(617, 333, 10);//填充 圆
	//Sleep(1000);

	setfillcolor(RED);
	fillcircle(554, 420, 35);//描边 圆
	line(554, 460, 554, 828);//直线
	//Sleep(1000);

	arc(320, 510, 789, 827, PI, 2 * PI);//绘制圆弧
	//Sleep(1000);

	line(125,313,296,410);
	line(83,444,270,474);
	line(83,594,262,527);
	line(819,414,990,320);
	line(845,478,1029,448);
	line(853,542,1029,660);
	getchar();//暂停
	closegraph();//关闭窗口
	return 0;
}