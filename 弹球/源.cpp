#include<iostream>
#include<easyx.h>
#include <conio.h>
using namespace std;
void ball()
{
	int x = 0, y = 0;
	int vx = 5, vy = 5;
	int r = 40;

	int barLeft, barTop, barRight, barBottom;
	barLeft = -150;
	barRight = 150;
	barTop = -280;
	barBottom = -300;

	while (1)
	{
		cleardevice();
		solidcircle(x, y, r);
		solidrectangle(barLeft, barTop, barRight, barBottom);
		Sleep(40);

		if (y >= 300 - r)
		{
			vy = -vy;
		}
		if (x <= -400 + r || x >= 400 - r)
		{
			vx = -vx;
		}
		if (barLeft <= x && x <= barRight && y <= barTop + r)
		{
			vy = -vy;
		}
		if (y <= -300)
		{
			x = rand() % (400 + 1) - 200;
			y = rand() % (300 + 1) - 150;
			vx = 5;
			vy = 5;
			if (rand() % 2 == 0)
			{
				vy = -vy;
			}
			if (rand() % 2 == 0)
			{
				vx = -vx;
			}
			barLeft = -150;
			barRight = 150;
			barTop = -280;
			barBottom = -300;
		}
		x += vx;
		y += vy;

		if (_kbhit() != 0)
		{
			char c = _getch();
			if (c == 'a')
			{
				if (barLeft > -400)
				{
					barLeft -= 20;
					barRight -= 20;
				}
			}
			if (c == 'd')
			{
				if (barRight < 400)
				{
					barLeft += 20;
					barRight += 20;
				}
			}
		}
	}
}
int main()
{
	srand(time(NULL));
	initgraph(800, 600);
	setorigin(400, 300);
	setaspectratio(1, -1);
	setbkcolor(RGB(164, 225, 202));
	cleardevice();

	ball();
	getchar();
	closegraph();
	return 0;
}