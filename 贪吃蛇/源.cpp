#include"标头.h"
int main()
{
	srand(size_t(time(NULL)));
	vector<node> snake = { {5,7},{4,7},{3,7},{2,7},{1,7} };
	initgraph(800, 600);
	char direction = 'w';
	node food = createFood(snake);
	while (1)
	{
		InitGraph();//初始化窗口
		paintSnake(snake);//画蛇
		paintFood(food);
		Sleep(500);

		if (_kbhit() != 0)
		{
			char tmp = _getch();
			if (tmp != EOF)
			{
				if((tmp=='a'&&direction!='d')||(tmp=='d')&& direction != 'a'||
				   (tmp == 'w' && direction != 's') || (tmp == 's') && direction != 'w')
				direction = tmp;
			}
		}//键盘输入
		node end = snake[snake.size() - 1];
		moveSnake(snake, direction);//移动蛇
		if (snake[0] == food)//如果吃到
		{
			snake.push_back(end);
			food = createFood(snake);
		}
		if (isGameOver(snake))
		{
			cout << "GameOver" << endl;
			break;
		}
	}
	closegraph();//关闭窗口
	return 0;
}