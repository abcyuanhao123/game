#include"标头.h"
void InitGraph()//初始化创建窗口
{
	//初始化窗口，设置颜色，刷新填充
	setbkcolor(RGB(164, 225, 202));
	cleardevice();

	for (int x = 0; x <= 800; x += LEN)
	{
		line(x, 0, x, 600);//画竖线
	}
	for (int y = 0; y <= 600; y += LEN)
	{
		line(0, y, 800, y);//画横线
	}
}
void paintSnake(vector<node>& snake)//绘制贪吃蛇
{
	int left, top, right, bottom;
	for (int i = 0; i < snake.size(); i++)
	{
		left = snake[i].x * LEN;
		top = snake[i].y * LEN;
		right = left + LEN;
		bottom = top + LEN;
		solidrectangle(left, top, right, bottom);//绘制单个方格，使用两对角坐标绘制
	}
}
void moveSnake(vector<node>& snake, char direction)//移动蛇
{
	snake.pop_back();//删尾 
	node newNode = snake[0];
	switch (direction)
	{
		case 'w':
		{
			newNode.y--;//上
			break;
		}
		case 's':
		{
			newNode.y++;//下
			break;
		}
		case 'a':
		{
			newNode.x--;//左
			break;
		}
		case 'd':
		{
			newNode.x++;//右
			break;
		}
	}
	snake.insert(snake.begin(),newNode);//更换头
}
node createFood(vector<node>& snake)//生成食物
{
	node food;
	while (1)
	{
		food.x = rand() % (800 / LEN);
		food.y = rand() % (600 / LEN);
		size_t i = 0;
		for (i = 0; i < snake.size(); i++)
		{
			if (snake[i].x == (food.x) && snake[i].y == (food.y))
			{
				break;
			}
		}
		if (i < snake.size())
			continue;
		else
			break;
	}
	return food;
}
void paintFood(node food)//画出食物
{
	int left, top, right, bottom;
	left = food.x * LEN;
	top = food.y * LEN;
	right = left + LEN;
	bottom = top + LEN;
	setfillcolor(YELLOW);
	solidrectangle(left, top, right, bottom);
	setfillcolor(WHITE);
}
bool isGameOver(const vector<node>& snake)//判断游戏是否结束
{
	if (snake[0].x < 0 || snake[0].x>800 / LEN)
		return true;
	if (snake[0].y < 0 || snake[0].y>600 / LEN)
		return true;
	for (int i = 1; i < snake.size(); i++)
	{
		if (snake[0].x == snake[i].x && snake[0].y == snake[i].y)
			return true;
	}
	return false;
}
