#pragma once
#include<iostream>
#include<vector>
#include<list>
#include<easyx.h>
#include<conio.h>
#include<time.h>
using namespace std;
#define LEN 40
struct node
{
	int x;
	int y;
	bool operator==(node& tmp)
	{
		return (x == tmp.x && y == tmp.y);
	}
};
void InitGraph();//初始化窗口
void paintSnake(vector<node>& snake);//绘制贪吃蛇
void moveSnake(vector<node>& snake, char direction);//移动贪吃蛇
node createFood(vector<node>& snake);//生成食物
void paintFood(node food);//画出食物
bool isGameOver(const vector<node>& snake);//判断游戏是否结束