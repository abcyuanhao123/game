#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<easyx.h>
using namespace std;
int main()
{
	LARGE_INTEGER startCount, endCount, F;//开始时间，结束时间，频率
	QueryPerformanceFrequency(&F);//获取计数器累加频率,（一秒加多少次）
	QueryPerformanceCounter(&startCount);//获取当前计数值
	Sleep(1000);//休眠一下
	QueryPerformanceCounter(&endCount);//获取终止计数值
	long long elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart ;//获取时间差，单位微秒
	/*cout << "start :" << startCount.QuadPart << endl;
	cout << "end :" << endCount.QuadPart << endl;
	cout << "end - start :" << endCount.QuadPart - startCount.QuadPart << endl;
	cout << F.QuadPart << endl;
	cout << elapse << endl;*/
	//BeginBatchDraw();//批量绘图开始
	while (1)
	{
		QueryPerformanceCounter(&startCount);//获取当前计数值

		//准备图片

		QueryPerformanceCounter(&endCount);//获取终止计数值

		long long elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;//获取时间差，单位微秒

		timeBeginPeriod(1);//调整系统的时钟分辨率，让Sleep函数的最小调用间隔为1毫秒，要不然默认为15毫秒
		while (elapse < 20000)//间隔时间小于20毫秒（每秒50针）
		{
			Sleep(1);//为了降低cpu使用
			QueryPerformanceCounter(&endCount);//获取终止计数值
			elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;
		}
		//FlushBatchDraw();//绘出准备好的图片
	}
	timeEndPeriod(1);//恢复时钟分辨率
	//EndBatchDraw();//批量绘图结束
	return 0;
}