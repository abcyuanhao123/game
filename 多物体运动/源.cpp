#include<iostream>
#include<easyx.h>
#include<vector>
#include <conio.h>
#include<time.h>
using namespace std;
#define BALL_NUM 100
class ball
{
public:
	ball()
		: x(rand() % (800 - 2 * r + 1) - 400 + r)
		, y(rand() % (600 - 2 * r + 1) - 300 + r)
		, v(rand() % 5 + 3)
		, theta(rand() % 360)
		, color(HSVtoRGB((float)(rand() % 360), 0.8f, 0.9f))
	{
		vx = cos(theta*3.14/180) * v;
		vy = sin(theta*3.14/180) * v;
	}
	void ball_move()//小球移动
	{
		if (y >= 300 - r||y<= -300 + r)
		{
			vy = -vy;
		}
		if (x <= -400 + r || x >= 400 - r)
		{
			vx = -vx;
		}
		x += vx;
		y += vy;
	}
	void painting_ball()
	{
		setfillcolor(color);
	    fillcircle(x,y,r);
	}
protected:
	int r = 10;
	int x , y;
	int v, vx, vy;
	double theta;
	COLORREF color;
};
void init_graph()
{
	initgraph(800, 600);
	setorigin(400, 300);
	setaspectratio(1, -1);
	setbkcolor(WHITE);
	setlinecolor(BLACK);
	cleardevice();
}
int main()
{
	srand(time(NULL));
	init_graph();//创建窗口
	vector<ball> arr;
	arr.resize(50);//100个球
	while (1)
	{
		BeginBatchDraw();//批量绘图开始
		//FlushBatchDraw()//显示目前存的图像，用来分此批量绘图
		for (auto& e : arr)
		{
			e.ball_move();
			e.painting_ball();
		}
		EndBatchDraw();//批量绘图结束，并把存的打印出来
		Sleep(40);
		cleardevice();
	}
	closegraph();
	return 0;
}