#include <iostream>
#include<easyx.h>
using namespace std;
void fun1()
{
	initgraph(800, 600);
	setbkcolor(WHITE);
	cleardevice();
	ExMessage msg;
	while (1)
	{
		msg = getmessage(EX_MOUSE);
		switch (msg.message)
		{
		case WM_MOUSEMOVE://鼠标移动
			setfillcolor(BLACK);
			solidcircle(msg.x, msg.y, 2);
			break;
		case WM_LBUTTONDOWN://鼠标左键
			setfillcolor(RED);
			if (msg.ctrl == true)//组合判断
				solidrectangle(msg.x - 10, msg.y - 10, msg.x + 10, msg.y + 10);
			else
				solidcircle(msg.x, msg.y, 10);
			break;
		case WM_RBUTTONDOWN:
			setfillcolor(BLUE);//鼠标右键
			solidcircle(msg.x, msg.y, 10);
			break;
		}
	}
}
int main()
{
	//ExMessage a, b, c;//消息结构体类型,来接收获取的消息
	//a = getmessage(-1);//获取消息函数：  -1，获取所有消息类别，鼠标消息，按键消息，字符消息，窗体消息
	///*								//EX_MOUSE	鼠标消息。
	//								//EX_KEY	按键消息。
	//								//EX_CHAR	字符消息。
	//								//EX_WINDOW	窗口消息。*/
	//b = getmessage(EX_MOUSE | EX_KEY);//消息之间可以通过|或按位运算来组合


	initgraph(800, 600,EX_SHOWCONSOLE);//第三个参数是打开控制台
	setbkcolor(WHITE);
	cleardevice();
	ExMessage msg;
	while (1)
	{
		msg = getmessage(EX_KEY);//仅获取键盘消息
		switch (msg.message)
		{
		case WM_KEYDOWN://键盘事件
			cout<<msg.vkcode<<endl;
			break;
		}
	}
	return 0;
}