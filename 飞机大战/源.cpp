#include"标头.h"
int main()
{
	initgraph(422, 705);//创建画布
	hearo a;
	boss bos;
	back_graph b;
	a.put_spirit();
	srand(time(NULL));
	BeginBatchDraw();//批量绘图开始

	LARGE_INTEGER startCount, endCount, F;//开始时间，结束时间，频率
	QueryPerformanceFrequency(&F);//获取计数器累加频率,（一秒加多少次）

	ExMessage msg;//鼠标信息获取

	int dcount = 0;// 死亡帧率记录
	int i = 0;
	while (1)
	{
		QueryPerformanceCounter(&startCount);//获取当前计数值
		cleardevice();//清空前面的窗口内容
		//准备图片

		b.back_move();//绘制背景图片
		QueryPerformanceCounter(&endCount);//获取终止计数值
		long long elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;//获取时间差，单位微秒
		timeBeginPeriod(1);//调整系统的时钟分辨率，让Sleep函数的最小调用间隔为1毫秒，要不然默认为15毫秒
		while (elapse < 20000)//间隔时间小于20毫秒（每秒50针）
		{
			// 鼠标消息获取,放到这里是让鼠标帧数多一些，画面延迟小
			peekmessage(&msg, EX_MOUSE);

			Sleep(1);//为了降低cpu使用
			QueryPerformanceCounter(&endCount);//获取终止计数值
			elapse = (endCount.QuadPart - startCount.QuadPart) * 1000000 / F.QuadPart;
		}

		a.move_spirit(msg);//鼠标信息确定英雄位置
		a.put_spirit();//画出英雄

		if (bos.life())//如果还活着，帧率记录器为0
		{
			bos.under_fire(a._list_bul);//判断是否受攻击
			bos.move_spirit();//移动
			bos.put_spirit();//绘画
		}
		else//代表死了
		{
			if (i<5 && dcount == 15)
			{
				i++;
				dcount = 0;
			}
			bos.death_animation(i);
			dcount++;
		}

		a.push_bullet(msg.x,msg.y);
		a.move_bullet();
		a.put_bullet();

		FlushBatchDraw();//绘出准备好的图片
	}

	timeEndPeriod(1);//恢复时钟分辨率
	EndBatchDraw();//批量绘图结束
	getchar();
	return 0;
}