#include"标头.h"
void spirit:: _putTransparentImage(int x, int y, IMAGE& mask, IMAGE& src)//画图
{
	putimage(x, y, &mask, SRCAND);//第三个参数是进行 三光栅计算 与背景图片与运算
	putimage(x, y, &src, SRCPAINT);//进行与运算
}
void spirit::put_spirit()//绘出飞机图片
{
	_putTransparentImage(_x, _y, _mask[_num/25], _src[_num/25]);//每0.5秒都是一种状态
	++_num;//第num绘制完毕开始下一个
	_num = _num % 50;
}

void spirit::move_spirit(ExMessage& msg)//进行飞机移动，根据鼠标消息进行坐标替换
{
	_x = msg.x - _wide / 2;
	_y = msg.y - _high / 2;//更新出左上角坐标
}
void spirit::put_bullet()//绘制子弹
{
	auto it = _list_bul.begin();
	while (it != _list_bul.end())
	{
		_putTransparentImage((*it)._xbul, (*it)._ybul, (*it)._mask_bul, (*it)._bul);
		it++;
	}//依次绘出链表里的所有子弹
}
void spirit::push_bullet(int x,int y)//根据射速添加子弹
{
	if (_quick_shoot++ >= 10)
	{
		_list_bul.push_back(bullet(x, y));//添加子弹
		_quick_shoot = 0;
	}
}
void spirit::move_bullet()//移动子弹
{
	auto it = _list_bul.begin();
	while (it != _list_bul.end())
	{
		it->_ybul -= 5;
		if (it->_ybul <= 0)
		{
			it = _list_bul.erase(it);//如果移动出界就删除
			continue;
		}
		it++;
	}//依次移动
}
void spirit::under_fire(list<bullet>& _list_bul)//敌人受攻击监测
{
	auto it = _list_bul.begin();
	while (it != _list_bul.end())
	{
		it->_ybul -= 5;
		if (it->_ybul >= _y&&it->_ybul<=_y+_high&&it->_xbul>=_x&&it->_xbul<=_x+_wide)//检测子弹范围
		{
			it = _list_bul.erase(it);//如果移动出界就删除
			_HP -= 10;
			continue;
		}
		it++;
	}//依次移动
}

hearo::hearo()//基类hearo初始化
{
	_x = 211 - 65 / 2;
	_y = 700 - 82;
	_high = 82;
	_wide = 65;
	//正常形态，摧毁形态图片初始化
	_mask.resize(2);
	_src.resize(2);
	for (int i = 0; i < _src.size(); i++)
	{
		char str[100];
		sprintf(str, "./img/hero/hero%d.png", i);
		loadimage(&_src[i], str);
		sprintf(str, "./img/hero/hero%d_mask.png", i);
		loadimage(&_mask[i], str);
	}

	_maskWrecked.resize(4);
	_srcWrecked.resize(4);
	for (int i = 0; i < _srcWrecked.size(); i++)
	{
		char str[100];
		sprintf(str, "./img/hero/hero_down%d.png", i);
		loadimage(&_srcWrecked[i], str);
		sprintf(str, "./img/hero/hero_down%d_mask.png", i);
		loadimage(&_maskWrecked[i], str);
	}
}
boss::boss()
{
	_x = rand() % (422-2*109) + 109;
	_y = 0;
	_high = 162;
	_wide = 109;
	//正常形态，摧毁形态图片初始化
	_mask.resize(1);
	_src.resize(1);
	for (int i = 0; i < _src.size(); i++)
	{
		char str[100];
		sprintf(str,"./img/enemy2/enemy2.png");
		loadimage(&_src[i], str);
		sprintf(str, "./img/enemy2/enemy2_mask.png");
		loadimage(&_mask[i], str);
	}

	_maskWrecked.resize(6);
	_srcWrecked.resize(6);
	for (int i = 0; i < _srcWrecked.size(); i++)
	{
		char str[100];
		sprintf(str, "./img/enemy2/enemy2_down%d.png", i);
		loadimage(&_srcWrecked[i], str);
		sprintf(str, "./img/hero/enemy2_down%d_mask.png", i);
		loadimage(&_maskWrecked[i], str);
	}
}
void boss::move_spirit()
{
	if (_num % 10 == 0)
	{
		_y += 1;
	}
	_num++;
}
void boss::put_spirit()
{
	_putTransparentImage(_x, _y, _mask[0], _src[0]);//每0.5秒都是一种状态
}
void boss::death_animation(int i)
{
	_putTransparentImage(_x, _y,  _maskWrecked[i], _srcWrecked[i]);//绘制第i种死亡状态
}