#pragma once
#define _CRT_SECURE_NO_WARNINGS 1
#include<iostream>
#include<easyx.h>
#include <conio.h>
#include<time.h>
#include<vector>
#include<queue>
#include<list>
using namespace std;
class bullet
{
public:
	bullet(int x=0,int y=0)//初始化子弹
	{
		_xbul = x;
		_ybul = y - 14;
		loadimage(&_bul, "./img/bullet/bullet.png");
		loadimage(&_mask_bul, "./img/bullet/bullet_mask.png");
	}
	bullet(const bullet& bul )//初始化子弹
	{
		_xbul = bul._xbul;
		_ybul = bul._ybul - 14;
		loadimage(&_bul, "./img/bullet/bullet.png");
		loadimage(&_mask_bul, "./img/bullet/bullet_mask.png");
	}
public:
	int _xbul;
	int _ybul;
	IMAGE _bul;
	IMAGE _mask_bul;
};
class spirit//精灵对象基类
{
public:
	//把对象画出来
	void _putTransparentImage(int x, int y, IMAGE& mask, IMAGE& src);
	virtual void put_spirit();//画出第num个hero，自动交替绘制
	virtual void move_spirit(ExMessage& msg);//根据鼠标位置更新飞机坐标
protected:
	int _x;//图片左上角坐标
	int _y;
	int _high;//图片的高
	int _wide;//图片的宽
	vector<IMAGE> _mask;//这四个存放图片信息
	vector<IMAGE> _src;
	vector<IMAGE> _maskWrecked;
	vector<IMAGE> _srcWrecked;

	//战斗属性
	int _quick_shoot = 25;//射速，该值每一帧++，到特定帧就push一个子弹
	int _HP = 100;//血量
	int _attack = 10;//攻击力
	int _num = 0;//交替进行,绘制移动动作
public:
	void under_fire(list<bullet>& _list_bul);//受到攻击的检测函数
	bool life()//判断是不是活着
	{
		return _HP > 0;
	}
	list<bullet> _list_bul;//子弹队列
public:
	void put_bullet();//画出子弹
	void push_bullet(int x, int y);//添加子弹
	void move_bullet();//子弹移动
};
class hearo : public spirit//英雄子类
{
public:
	hearo();//初始化图片，坐标
};
class boss : public spirit//英雄子类
{
public:
	boss();//初始化图片，坐标
	void move_spirit();
	void put_spirit();
	void death_animation(int i=0);//播放毁坏动画
};
//背景类完成
class back_graph
{
public:
	back_graph();//初始化
	void back_move();//不断移动背景图片，并绘制出来
protected:
	int _wide = 422;//图片的宽
	int _high=750;//图片的高
	IMAGE _back1;//背景1
	int _x1=0, _y1=0;
	IMAGE _back2;//背景2
	int _x2=_x1, _y2=-_high;
};

