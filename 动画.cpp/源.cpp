#include<iostream>
#include<easyx.h>
using namespace std;
#define PI 3.14
void fivePointedStar(int x, int y, int radius, double starAngle)
{
	double delta = 2 * PI / 5;
	POINT points[5];
	for (int i = 0; i < 5; i++)
	{
		points[i].x = cos(starAngle + i*delta*2)*radius+x;
		points[i].y = sin(starAngle + i*delta*2)*radius+y;
	}
	solidpolygon(points, 5);
}
int main()
{
	initgraph(800, 600);
	setorigin(400, 300);
	setaspectratio(1, -1);

	setbkcolor(RGB(164, 225, 202));
	cleardevice();

	setfillcolor(WHITE);
	setpolyfillmode(WINDING);
	int x, y;
	x = -400;
	y = 0;
	int dx = 5;
	while (1)
	{
		cleardevice();
		fivePointedStar(x, y, 50, 0);
		Sleep(40);
		x = x + dx;
		if (x == -400 || x == 400)
		{
			dx = -dx;
		}
	}
	getchar();
	closegraph();
	return 0;
}